import React, { Component } from 'react';

import Task from "./component/task/task";
import AddFromNameSuum  from  './component/AddFormNameSum/AddFromNameSum'

import './App.css';


class App extends Component {
  state = {
    items:[],
    value: '',
    valueSum: '',
    totalPrice: 0
  };
  AddTaskHandler = () =>{
     let newItems = {item: this.state.value, cost: this.state.valueSum};
     let  items = [...this.state.items, newItems];
     const price = this.state.totalPrice;
     const itemAdd = parseInt(this.state.valueSum);
     const newPrice = price + itemAdd;

     this.setState({items, value: '',  totalPrice: newPrice})
  };
  inputValue = (value)=> {
    this.setState({value})
  };
    inputValueSum = (valueSum)=> {
        this.setState({valueSum})
    };

    removeItem = (value,cost) => {

    let items = [...this.state.items];

    const oldPrice = this.state.totalPrice;

    const newPrice = oldPrice - parseInt(cost);
    const index = items.findIndex((item) => item.item === value);
        items.splice(index,1);

        this.setState({items: items, totalPrice: newPrice})
    };
  render() {
    return (
      <div className="App">
          <AddFromNameSuum value={this.inputValue} valSum={this.inputValueSum} addItems={this.AddTaskHandler}/>
          <div className="border-text">
              {this.state.items.map((item, id) => <Task item={item.item} key={id} cost={item.cost} removed={(value, cost) => this.removeItem(value,cost)}/>) }
              <div>
                  <p> total spend {this.state.totalPrice}</p>
              </div>
          </div>

      </div>

    );
  }
}

export default App;
